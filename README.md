# typescript-transformer-append-js-extension

## Info

Typescript transformer to add .js extensions to all imported files.

Require NodeJS 16+

## Example

```
├─ /files
│ ├─ index.ts
│ └─ file1.ts
└─ main.ts
```

#### Source files

```
// package.json
{ "type": "module" }
```

```ts
// src/main.ts
import { a } from './files';        // folder
import { b } from './files/file1';
console.log(a + b);

// src/files/index.ts
export * from './file1.ts';

// src/files/file1.ts
export const a = 1;
export const b = 2;
```

#### Output without `typescript-transformer-append-js-extension`

```js
// dist/main.js
import { a } from './files';
import { b } from './files/file1';
console.log(a + b);

// dist/files/index.js
export * from './file1';

// dist/files/file1.js
export const a = 1;
export const b = 2;
```

```
>> node dist/main.js
Error [ERR_UNSUPPORTED_DIR_IMPORT]: Directory import 'dist\files' is not supported resolving ES modules imported from dist\main.js
```

#### Output with `typescript-transformer-append-js-extension`

```js
// dist/main.js
import { a } from './files/index.js';
import { b } from './files/file1.js';
console.log(a + b);

// dist/files/index.js
export * from './file1.js';

// dist/files/file1.js
export const a = 1;
export const b = 2;
```

```
>> node dist/main.js
3
```
