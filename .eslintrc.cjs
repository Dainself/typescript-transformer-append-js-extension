module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    // project: 'tsconfig.json',
    ecmaVersion: 2020,
    sourceType: 'module',
  },
  env: {
    node: true,
    jest: true,
  },
  rules: {
    'arrow-parens': ['error', 'always'],
    'brace-style': ['error', 'stroustrup', {
      'allowSingleLine': false,
    }],
    'comma-dangle': ['error', 'always-multiline'],
    'curly': 'error',
    'eol-last': ['error', 'always'],
    'indent': ['error', 2, {
      'SwitchCase': 1,
    }],
    'keyword-spacing': 'error',
    'lines-between-class-members': 'off',
    'max-classes-per-file': ['error', 1],
    'no-multiple-empty-lines': ['error', { 'max': 3, 'maxEOF': 0 }],
    'no-var': 'error',
    'object-curly-spacing': ['error', 'always'],
    'padding-line-between-statements': ['error', {
      'blankLine': 'always',
      'prev': '*',
      'next': ['let', 'const'],
    }, {
      'blankLine': 'always',
      'prev': ['let', 'const'],
      'next': '*',
    }, {
      'blankLine': 'any',
      'prev': ['let', 'const'],
      'next': ['let', 'const'],
    }, {
      'blankLine': 'always',
      'prev': '*',
      'next': 'return',
    }],
    'quotes': ['error', 'single'],
    'semi': ['error', 'always'],
    'space-before-function-paren': ['error', {
      'anonymous': 'always',
      'named': 'never',
      'asyncArrow': 'always',
    }],

    // 'promise/param-names': 'off',
  },
  overrides: [
    {
      files: ['*.d.ts'],
      rules: {
        'max-classes-per-file': 'off',
      },
    },
    {
      files: ['*.ts'],
      plugins: [
        '@typescript-eslint/eslint-plugin',
      ],
      rules: {
        '@typescript-eslint/adjacent-overload-signatures': 'error',
        '@typescript-eslint/consistent-type-assertions': ['error', {
          assertionStyle: 'as',
          objectLiteralTypeAssertions: 'allow',
        }],
        '@typescript-eslint/explicit-member-accessibility': 'error',
        '@typescript-eslint/member-delimiter-style': ['error', {
          multiline: {
            delimiter: 'semi',
            requireLast: true,
          },
          singleline: {
            delimiter: 'semi',
            requireLast: false,
          },
        }],
        '@typescript-eslint/naming-convention': ['error',
          { selector: 'default', format: ['camelCase'], leadingUnderscore: 'allow' },
          { selector: 'variable', modifiers: ['const'], format: ['camelCase', 'UPPER_CASE'] },
          { selector: 'typeLike', format: ['PascalCase'] },
          { selector: 'objectLiteralProperty', format: ['camelCase', 'UPPER_CASE'] },
          { selector: 'objectLiteralProperty', modifiers: ['requiresQuotes'], format: null },
        ],
        '@typescript-eslint/no-namespace': ['error', {
          'allowDefinitionFiles': true,
        }],
        '@typescript-eslint/triple-slash-reference': ['error', {
          'path': 'never',
          'types': 'never',
          'lib': 'never',
        }],
        '@typescript-eslint/type-annotation-spacing': 'error',
      },
    },
  ],
};
