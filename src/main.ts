import path from 'path';
import ts from 'typescript';


export default (program: ts.Program) => {
  let compilerOptions = program.getCompilerOptions();

  return (transformationContext: ts.TransformationContext) => (sourceFile: ts.SourceFile) => {
    /** @type { ts.NodeFactory } */
    let factory = transformationContext.factory;
    let srcFilePath = sourceFile.fileName;
    // @ts-ignore
    let srcFileDir = ts.normalizePath(path.dirname(srcFilePath));

    function visitNode(node: ts.Node): ts.Node {
      if (ts.isImportDeclaration(node)) {
        if (node.moduleSpecifier && ts.isStringLiteral(node.moduleSpecifier)) {
          return factory.updateImportDeclaration(
            node, node.decorators, node.modifiers, node.importClause,
            factory.createStringLiteral(resolveModulePath(node.moduleSpecifier.text)),
          );
        }
      }
      else if (ts.isExportDeclaration(node)) {
        if (node.moduleSpecifier && ts.isStringLiteral(node.moduleSpecifier)) {
          return factory.updateExportDeclaration(
            node, node.decorators, node.modifiers, node.isTypeOnly, node.exportClause,
            factory.createStringLiteral(resolveModulePath(node.moduleSpecifier.text)),
          );
        }
      }
      else if (isRequire(node) || isAsyncImport(node)) {
        return factory.updateCallExpression(
          node, node.expression, node.typeArguments,
          [factory.createStringLiteral(resolveModulePath((node.arguments[0] as ts.StringLiteral).text))],
        );
      }

      return ts.visitEachChild(node, visitNode, transformationContext);
    }

    function resolveModulePath(origModulePath: string) {
      let { resolvedModule } = ts.resolveModuleName(origModulePath, srcFilePath, compilerOptions, ts.sys);

      if (!resolvedModule || resolvedModule.isExternalLibraryImport) {
        return origModulePath;
      }

      let { extension, resolvedFileName } = resolvedModule;
      let modulePath = path.dirname(resolvedFileName);
      let ext = '.js';
      // @ts-ignore
      let res = ts.normalizePath(path.join(path.relative(srcFileDir, modulePath), path.basename(resolvedFileName, extension) + ext));

      return res[0] === '.' ? res : `./${res}`;
    }

    return ts.visitNode(sourceFile, visitNode) as ts.Node;
  };
};


function isRequire(node: ts.Node): node is ts.CallExpression {
  return ts.isCallExpression(node)
    && ts.isIdentifier(node.expression)
    && node.expression.text === 'require'
    && ts.isStringLiteral(node.arguments[0])
    && node.arguments.length === 1;
}

function isAsyncImport(node: ts.Node): node is ts.CallExpression {
  return ts.isCallExpression(node)
    && node.expression.kind === ts.SyntaxKind.ImportKeyword
    && ts.isStringLiteral(node.arguments[0])
    && node.arguments.length === 1;
}
